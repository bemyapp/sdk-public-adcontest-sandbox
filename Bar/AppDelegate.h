//
//  AppDelegate.h
//  Bar
//
//  Created by Julien Sarazin on 07/03/13.
//  Copyright (c) 2013 BeMyApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
