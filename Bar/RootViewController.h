//
//  ViewController.h
//  Bar
//
//  Created by Julien Sarazin on 07/03/13.
//  Copyright (c) 2013 BeMyApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdContestDelegate.h"

@interface RootViewController : UIViewController <AdContestDelegate>

- (IBAction)launchBanner:(id)sender;
- (IBAction)checkUserRegistration:(id)sender;
- (IBAction)submitScore:(id)sender;
- (IBAction)launchDashboard:(id)sender;
- (IBAction)launchFullScreen:(id)sender;


@property (weak, nonatomic) IBOutlet UITextField *scoreField;
@property (weak, nonatomic) IBOutlet UILabel *lblUserRegistered;

@end
