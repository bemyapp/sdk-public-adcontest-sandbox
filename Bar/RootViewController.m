//
//  ViewController.m
//  Bar
//
//  Created by Julien Sarazin on 07/03/13.
//  Copyright (c) 2013 BeMyApp. All rights reserved.
//

#import "RootViewController.h"
#import "AdContest.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	_scoreField.keyboardType = UIKeyboardTypeNumberPad;
	
	NSLog(@"Using  Ad Contest SDK - version: %i", [AdContest versionNumber]);
	
	// Allow to display fullscreen each 30sec.
	[AdContest setFsRate:30];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)launchBanner:(id)sender
{
	[AdContest launchBannerWithController:self withDelegate:self];
}

- (IBAction)checkUserRegistration:(id)sender
{
	_lblUserRegistered.text = [AdContest isUserRegisteredForCampaign] ? @"VALID" : @"INVALID";
}

- (IBAction)submitScore:(id)sender
{
	if([_scoreField.text isEqualToString:@""])
		return;
	
	[AdContest saveScore:[_scoreField.text intValue]];
	[_scoreField resignFirstResponder];
}

- (IBAction)launchDashboard:(id)sender
{
	[AdContest launchDashboardWithDelegate:self];
}

- (IBAction)launchFullScreen:(id)sender
{
	[AdContest launchFullScreenWithDelegate:self];
}

#pragma mark - AdConstest delegate methods -
- (void)userPressPlayGameButton:(NSString *)userId
{
	NSLog(@"User id: %@", userId);
}

- (void)dashboardWillAppear
{
	NSLog(@"Dashboard will appear");
}

- (void)dashboardDidAppear
{
	NSLog(@"Dashboard did apprear.");
}

- (void)dashboardWillDisappear
{
	NSLog(@"Dashboard will disappear.");
}

- (void)dashboardDidDisappear
{
	NSLog(@"Dashboard did disappear.");
}

- (void)offlineUserLoggedIn:(NSString *)userId
{
	NSLog(@"OffLine loggin with userID : %@", userId);
}

- (void)userLoggedIn:(NSString *)userId
{
	NSLog(@"User Logged in with userID: %@", userId);
}

@end
