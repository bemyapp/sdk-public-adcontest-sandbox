//
//  main.m
//  Bar
//
//  Created by Julien Sarazin on 07/03/13.
//  Copyright (c) 2013 BeMyApp. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
