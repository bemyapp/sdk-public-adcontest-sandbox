##ADContest SDK Guideline
This short documentation will explain how to integrate the SDK in your application.

#Requirements
To use the SDK in your application you must include some librairies :

- CFNetwork
- MobileCoreService
- Libz.dylib

You also need to add an option in your build settings :

Other linker flags : -all_load

#Initialisation:
``` objective-c
+ (void)initializeWithProductKey:(NSString *)productKey andDelegate(id<ADContestDelegate>);

[AdContest initializeWithProductKey:@"ThisIsMyProduct1"];
```

#Banner creation:
``` objective-c
+ (void)launchBannerWithController:(UIViewController *) controller withDelegate:(id<AdContestDelegate>) delegate;
[AdContest launchBannerWithController:self withDelegate:self];
```

#Check user registration:
``` objective-c
+ (bool)isUserRegisteredForCampaign;
[AdContest isUserRegisteredForCampain];
```

#Submit score:
``` objective-c
+ (bool)saveScore:(int)score;
[AdContest saveScore:5];
```