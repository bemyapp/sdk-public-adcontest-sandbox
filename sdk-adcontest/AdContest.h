//
//  AdContest.h
//  AdContestLib
//
//  Created by Michel Ho Fong Fat on 8/16/11.
//  Copyright 2011 Michel Ho Fong Fat. All rights reserved.
//

#import "AdContestDelegate.h"

// for MD5 signing
#import <CommonCrypto/CommonDigest.h>

#define USESERVERPRODUCTION 1

//#if USESERVERPRODUCTION

#define ACCONTESTMAXLOCALHIGHSCORE  20
#define BMAHOST                     @"http://bemyapp.com"
#define ADCONTESTURLIMGPATH         @"http://bemyapp.com/iphone/img/contests"
#define ADCONTESTURLGETCAMPAIGN     @"http://bemyapp.com/adcontest/getcampaign.php"
#define ADCONTESTURLNEWPLAYER       @"http://bemyapp.com/adcontest/newplayer.php"
#define ADCONTESTURLLOGIN           @"http://bemyapp.com/adcontest/login.php"
#define ADCONTESTURLPOSTSCORE       @"http://bemyapp.com/adcontest/postscore.php"
#define ADCONTESTURLGETPLAYERRANK   @"http://bemyapp.com/adcontest/getplayerrank.php"
#define ADCONTESTURLGETLEADERBOARD  @"http://bemyapp.com/adcontest/getleaderboard.php"

//#else


//#endif

#define AC_OPTIONALLY_INVOKE_DELEGATE(_delegate, _selector)	\
{ \
id delegateAsId = (id)_delegate;								\
if([delegateAsId respondsToSelector:@selector(_selector)])	\
{														\
[delegateAsId performSelector:@selector(_selector)];	\
} \
}

#define AC_OPTIONALLY_INVOKE_DELEGATE_WITH_PARAMETER(_delegate, _selector, _parameter)	\
{ \
id delegateAsId = (id)_delegate;								\
if([delegateAsId respondsToSelector:@selector(_selector)])	\
{														\
[delegateAsId performSelector:@selector(_selector) withObject:(id)(_parameter)];	\
} \
}

#define AC_OPTIONALLY_INVOKE_DELEGATE_WITH_TWO_PARAMETERS(_delegate, _selector, _parameterOne, _parameterTwo)	\
{ \
id delegateAsId = (id)_delegate;								\
if([delegateAsId respondsToSelector:@selector(_selector)])	\
{														\
[delegateAsId performSelector:@selector(_selector) withObject:(id)(_parameterOne) withObject:(id)(_parameterTwo)];	\
} \
}

//////////////////////////////////////////////////////////////////////////
/// Defines a macro which releases an objective C class and sets it to nil
//////////////////////////////////////////////////////////////////////////
#define ACSafeRelease(ocObject) [ocObject release]; \
ocObject = nil;


#pragma mark -
#pragma mark Static NSString Definition

@class AdContestContactingServerController;
//@class AdContestUserRegistrationController;
//@class AdContestCampaignController;

@class ADC_ASIHTTPRequest;
@class AdContestRootController;
@class CampaignView;
@class RegisterView;
@class AdContestFullScreenController;
@class AdContestNoCampaignController;
@class AdContestWaitingForApprovalController;

@interface AdContest : NSObject<UIWebViewDelegate>
{
	id<AdContestDelegate> mLaunchDelegate;
    
    AdContestRootController *mAdContestRootController;
    
    //AdContestUserRegistrationController *mUserRegistrationController;
    RegisterView *mUserRegistrationController;
    AdContestFullScreenController *mFullScreenController;
    //AdContestCampaignController *mCampaignController;
    CampaignView *mCampaignController;
    AdContestNoCampaignController *mNoCampaignController;
    AdContestWaitingForApprovalController *mWaitingForApprovalController;
    
	bool mSuccessfullyBootstrapped;

    NSString *mProductID;
    NSString *mProductKey;
    NSString *mProductName;
    
    NSString *mContestID;
    NSString *mContestTitle;
    NSString *mContestDescription;
    NSString *mContestUrlMoreInformation;
    NSString *mContestUrlTerms;
    NSString *mContestDateStart;
    NSString *mContestDateEnd;
    
    UIButton *mBannerButton;
    UIWebView *mBannerWebView;
    
	/// md5 context
	CC_MD5_CTX		md5Ctx;
    
    float mCampaignViewBackgroundColorRedValue;
    float mCampaignViewBackgroundColorGreenValue;
    float mCampaignViewBackgroundColorBlueValue;
    
    @private bool mHasCampaign;
}

@property (nonatomic, assign) id<AdContestDelegate> mLaunchDelegate;

@property (nonatomic, retain) AdContestRootController *mAdContestRootController;

//@property (nonatomic, retain) AdContestUserRegistrationController *mUserRegistrationController;
@property (nonatomic, retain) RegisterView *mUserRegistrationController;
//@property (nonatomic, retain) AdContestCampaignController *mCampaignController;
@property (nonatomic, retain) CampaignView *mCampaignController;
@property (nonatomic, retain) AdContestFullScreenController *mFullScreenController;
@property (nonatomic, retain) AdContestNoCampaignController *mNoCampaignController;
@property (nonatomic, retain) AdContestWaitingForApprovalController *mWaitingForApprovalController;

@property (nonatomic, retain) NSString *mProductID;
@property (nonatomic, retain) NSString *mProductKey;
@property (nonatomic, retain) NSString *mProductName;

@property (nonatomic, retain) NSString *mContestID;
@property (nonatomic, retain) NSString *mContestTitle;
@property (nonatomic, retain) NSString *mContestDescription;
@property (nonatomic, retain) NSString *mContestUrlMoreInformation;
@property (nonatomic, retain) NSString *mContestUrlTerms;
@property (nonatomic, retain) NSString *mContestDateStart;
@property (nonatomic, retain) NSString *mContestDateEnd;

@property (nonatomic, readwrite) float mCampaignViewBackgroundColorRedValue;
@property (nonatomic, readwrite) float mCampaignViewBackgroundColorGreenValue;
@property (nonatomic, readwrite) float mCampaignViewBackgroundColorBlueValue;

@property (nonatomic, retain) UIButton *mBannerButton;
@property (nonatomic, retain) UIWebView *mBannerWebView;

////////////////////////////////////////////////////////////
///
/// @return The version of the AdContest client library in use.
///
////////////////////////////////////////////////////////////
+ (NSUInteger)versionNumber;

////////////////////////////////////////////////////////////
///
/// @return The release Version String of the AdContest client library in use.
///
////////////////////////////////////////////////////////////
+ (NSString*)releaseVersionString;

////////////////////////////////////////////////////////////
///
/// @param productKey is copied. This is your unique product key you received when registering your application.
/// @param productSecret is copied. This is your unique product secret you received when registering your application.
/// @param displayName is copied.
///
/// @note This will begin the application authorization process.
///
////////////////////////////////////////////////////////////
+ (void)initializeWithProductKey:(NSString *)productKey 
                       andSecret:(NSString *)secret
                  andDisplayName:(NSString *)displayName;

+ (void)initializeWithProductKey:(NSString *)productKey;

+ (void)initializeWithProductKey:(NSString *)productKey
                        andDelegate:(id<AdContestDelegate>)delegate;

////////////////////////////////////////////////////////////
///
/// Shuts down AdContest
///
////////////////////////////////////////////////////////////
+ (void)shutdown;

////////////////////////////////////////////////////////////
///
/// Display banner
///
////////////////////////////////////////////////////////////
+ (void)launchBannerWithController:(UIViewController *) controller withDelegate:(id<AdContestDelegate>) delegate;

////////////////////////////////////////////////////////////
///
/// @see launchFullScreen
///
////////////////////////////////////////////////////////////
+ (void)launchFullScreenWithDelegate:(id<AdContestDelegate>)delegate;

////////////////////////////////////////////////////////////
///
/// @see launchDashboard
///
////////////////////////////////////////////////////////////
+ (void)launchDashboardWithDelegate:(id<AdContestDelegate>)delegate;

////////////////////////////////////////////////////////////
///
/// Removes the AdContest Dashboard from your application's keyed window.
///
////////////////////////////////////////////////////////////
+ (void)dismissDashboard;

+ (bool)isShowing;

////////////////////////////////////////////////////////////
///
/// Returns whether or not the user has enabled AdContest for this game.
///
////////////////////////////////////////////////////////////
+ (bool)isUserRegisteredForCampaign;

////////////////////////////////////////////////////////////
///
/// Submit a score
///
////////////////////////////////////////////////////////////
+ (bool)saveScore:(int)score;
////////////////////////////////////////////////////////////
///
/// @param rate the fullscreen display rate in seconds (default 120s, min 30s)
///
////////////////////////////////////////////////////////////
+(void)setFsRate:(long) rate;



@end
